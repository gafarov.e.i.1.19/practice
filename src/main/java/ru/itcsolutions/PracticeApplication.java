package ru.itcsolutions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeApplication {

    /**
     * Это основной метод. Как видите он стартует SpringApplication, это нюанс SpringBoot.
     * Данное приложение построено на Spring
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(PracticeApplication.class, args);
    }

}
