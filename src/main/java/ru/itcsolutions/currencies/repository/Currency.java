package ru.itcsolutions.currencies.repository;

/**
 * Описание валюты
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public interface Currency {
    public int getCurrencyCodeNum();
    public Double getCourseFromUSD();
    public String getCurrencyCode();
    public Double convertFromUSD(Double usdAmount);
}
