package ru.itcsolutions.distance.repository.model;

import ru.itcsolutions.distance.repository.MetricSystem;

/**
 * Расстояния в метрах/километрах
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public class MetresDistanceMetricSystem implements MetricSystem {

    private String name;
    private Double indexFromMetres;

    public MetresDistanceMetricSystem() {
        this.name = "Метрическая система измерения (метры)";
        this.indexFromMetres = new Double(1);
    }

    @Override
    public Double getConvertIndexFromMetres() {
        return this.indexFromMetres;
    }

    @Override
    public String getMetricSystemName() {
        return this.name;
    }

    @Override
    public Double convertFromMetres(Double metresAmount) {
        return metresAmount;
    }

    @Override
    public Double convertFromKilometres(Double kilometresAmount) {
        return kilometresAmount / 1000;
    }
}
