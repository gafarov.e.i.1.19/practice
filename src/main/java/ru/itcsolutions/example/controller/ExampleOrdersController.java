package ru.itcsolutions.example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import ru.itcsolutions.example.dto.DataTransferObject;
import ru.itcsolutions.example.repository.ExampleOrder;
import ru.itcsolutions.example.service.ExampleOrdersService;

import java.util.List;

/**
 * Пример контроллера
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
@Component
@RestController
@RequestMapping("/example")
public class ExampleOrdersController {

    private final ExampleOrdersService exampleOrdersService;

    /*
     Конструктор
     */
    public ExampleOrdersController(ExampleOrdersService exampleOrdersService) {
        this.exampleOrdersService = exampleOrdersService;
    }

    /**
     * Получить все заказы
     * @return список всех заказов
     */
    @GetMapping("/orders/all")
    public ResponseEntity<List<ExampleOrder>> getAll() {
        return ResponseEntity.ok(exampleOrdersService.getAllOrders());
    }

    /**
     * Получить заказ по идентификатору
     * @param orderId - идентификатор заказа
     * @return заказ по идентификатору
     */
    @GetMapping("/orders/{id}")
    public ResponseEntity<ExampleOrder> getOrder(@PathVariable("id") Long orderId) {
        try {
            return ResponseEntity.ok(exampleOrdersService.getOrder(orderId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    /**
     * Обновить или создать новый заказ
     * @param orderId - идентификатор заказа
     * @param dataTransferObject - объект с данными заказа
     * @return обновленный заказ
     */
    @PutMapping("/orders/{id}")
    public ResponseEntity<ExampleOrder> addOrUpdateOrder(
            @PathVariable("id") Long orderId,
            @RequestBody DataTransferObject dataTransferObject) {
        try {
            return ResponseEntity.ok(exampleOrdersService.addOrUpdateOrder(orderId, dataTransferObject));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    /**
     * Удалить заказ
     * @param orderId - идентификатор заказа
     * @return ничего
     */
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable("id") Long orderId) {
        exampleOrdersService.deleteOrder(orderId);
        return ResponseEntity.ok().build();
    }
}
