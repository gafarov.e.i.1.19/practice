package ru.itcsolutions.example.dto;


/**
 * Пример данных для передачи/получения данных извне
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public class DataTransferObject {
    private String type;
    private String name;
    private int cost;
    private boolean isInStock;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isInStock() {
        return isInStock;
    }

    public void setInStock(boolean inStock) {
        isInStock = inStock;
    }
}
