package ru.itcsolutions.example.repository;

import java.time.LocalDateTime;

/**
 * Интерфейс, описывающий методы заказа
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public interface ExampleOrder {

    /**
     * Вернуть идентифкатор заказа
     * @return идентифкатор заказа
     */
    public Long getOrderId();

    /**
     * Вернуть название заказа
     * @return название заказа
     */
    public String getName();

    /**
     * Вернуть идентифкатор заказа
     * @return идентифкатор заказа
     */
    public LocalDateTime getUpdateTime();

    /**
     * Вернуть стоимость заказа
     * @return стоимость заказа
     */
    public int getCost();

    /**
     * Вернуть информацию о наличии заказа
     * @return информация о наличии заказа
     */
    public boolean isInStock();


    /**
     * Установить дату обновления
     * @param localDateTime  дата обнволения
     */
    public void setUpdateTime(LocalDateTime localDateTime);


    /**
     * Установить идентификатор
     * @param orderId идентификатор
     */
    public void setOrderId(Long orderId);
}
