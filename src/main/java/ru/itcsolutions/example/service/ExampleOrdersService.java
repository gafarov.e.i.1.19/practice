package ru.itcsolutions.example.service;

import org.springframework.stereotype.Service;
import ru.itcsolutions.example.dto.DataTransferObject;
import ru.itcsolutions.example.repository.model.BookOrder;
import ru.itcsolutions.example.repository.model.DressOrder;
import ru.itcsolutions.example.repository.ExampleOrder;
import ru.itcsolutions.example.repository.ExampleOrdersRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Пример сервиса
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
@Service
public class ExampleOrdersService {

    private final ExampleOrdersRepository exampleOrdersRepository;

    /*
     Конструктор
     */
    public ExampleOrdersService(ExampleOrdersRepository exampleOrdersRepository) {
        this.exampleOrdersRepository = exampleOrdersRepository;
    }

    /**
     * Получить список закзаов
     * @param orderId - идентифкатор
     * @return преобразованный к передаче данных объект
     */
    public ExampleOrder getOrder(Long orderId) throws Exception {
        List<ExampleOrder> exampleOrders = exampleOrdersRepository.getOrders();
        for (ExampleOrder exampleOrder: exampleOrders) {
            if (exampleOrder.getOrderId().longValue() == orderId) {
                return exampleOrder;
            }
        }
        throw new Exception("Заказ с идентификатором не найден " + orderId);
    }

    /**
     * Добавить обновить заказ
     * @param orderId - идентификатор
     * @param dataTransferObject - данные заказа
     * @return новый/обновленный заказ
     */
    public ExampleOrder addOrUpdateOrder(Long orderId, DataTransferObject dataTransferObject) throws Exception {

        // удаляем предыдущие записи по идентификатору
        deleteOrder(orderId);

        // получаем свежий спсиок после удлаения
        List<ExampleOrder> exampleOrders = exampleOrdersRepository.getOrders();

        //обновлям данные в модели
        if (dataTransferObject.getType() == null) {
            throw new Exception("Не указан тип заказа");
        }

        ExampleOrder newOrUpdatedExampleOrder;
        if (dataTransferObject.getType().equalsIgnoreCase("book")) {
            newOrUpdatedExampleOrder = new BookOrder(
                    orderId,
                    dataTransferObject.getName(),
                    LocalDateTime.now(),
                    dataTransferObject.getCost(),
                    dataTransferObject.isInStock()
            );
        } else {
            newOrUpdatedExampleOrder = new DressOrder(
                    orderId,
                    dataTransferObject.getName(),
                    LocalDateTime.now(),
                    dataTransferObject.getCost(),
                    dataTransferObject.isInStock()
            );
        }

        //обновлем в списке
        exampleOrders.add(newOrUpdatedExampleOrder);
        exampleOrdersRepository.setOrders(exampleOrders);
        return newOrUpdatedExampleOrder;
    }

    /**
     * Удалить заказ по идентификатору
     * @param orderId - идентификатор
     */
    public void deleteOrder(Long orderId) {
        List<ExampleOrder> exampleOrders = exampleOrdersRepository.getOrders();

        // удаляем предыдущие записи по идентификатору
        List<ExampleOrder> newExampleOrders = new ArrayList<>();
        for (ExampleOrder exampleOrder: exampleOrders) {
            if (exampleOrder.getOrderId().longValue() != orderId) {
                newExampleOrders.add(exampleOrder);
            }
        }
        //обновлем в списке
        exampleOrdersRepository.setOrders(newExampleOrders);
    }

    /**
     * Вернуть все заказы
     * @return - спсиок заказов
     */
    public List<ExampleOrder> getAllOrders() {
        return exampleOrdersRepository.getOrders();
    }
}
