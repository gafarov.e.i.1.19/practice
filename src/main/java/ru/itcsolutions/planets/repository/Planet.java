package ru.itcsolutions.planets.repository;

import java.util.List;

/**
 * Планета
 *
 * @author mikhailyuk
 * @since 16.06.2021
 */
public class Planet {
    private Long id;
    private String name;
    private List<String> satellites;
    private List<String> missions;

    public Planet(Long id, String name, List<String> satellites, List<String> missions) {
        this.id = id;
        this.name = name;
        this.satellites = satellites;
        this.missions = missions;
    }

    public Planet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSatellites() {
        return satellites;
    }

    public void setSatellites(List<String> satellites) {
        this.satellites = satellites;
    }

    public List<String> getMissions() {
        return missions;
    }

    public void setMissions(List<String> missions) {
        this.missions = missions;
    }
}
